﻿using System;

namespace Salario
{
    class Program
    {
        static void Main(string[] args)
        {
            double salarioBruto;
            double premio;
            double baseIAPAS;
            double baseIR;
            double taxaIAPAS;
            double taxaIR;
            double salario;
            double impostoIAPAS = 0;
            double resultadoSalario;
            double impostoIR = 0;
            
            
            
            salarioBruto = 45000;
            premio = 10000;
            taxaIAPAS = 0.1;
            taxaIR = 0.15;
            baseIAPAS = 30000;
            baseIR = 50000;
            salario = salarioBruto + premio;
            
            if (salario > baseIAPAS)
            {
                impostoIAPAS = salario * taxaIAPAS;
                
            }
            
            

            if (salario - impostoIAPAS > baseIR)
            {
                impostoIR = taxaIR * (salario - impostoIAPAS);
            }

            resultadoSalario = salario - impostoIAPAS - impostoIR;

            Console.WriteLine(resultadoSalario);
            
        }
    }
}