## Enunciado
Numa pequena empresa, cada funcionário recebe mensalmente o ordenado mais um prêmio, referente a comissões.
São dados os seguintes valores: salário bruto prêmio, além dos valores da base do IAPAS , base do imposto de renda e
respectivas taxas (IAPAS o IR). Daneja-ne emitir a demonstrativo de pagamento (hollerith) de um funcionário, contendo os
valores do salário bruto, prêmio, desconto IAPAS, desconto IR salário líquido. 

## Esclarecimento
O rendimento do
funcionário é a soma do salário bruto com o prêmios; o desconto do IAPAS somente incide se o rendimento for maior do que
a base do IAPAS e o desconto do imposto de renda, somente se a diferença entre os valores de rendimento e o valor do
desconto do TAPAS for maior do que a base do Imposto de Renda.

---

Vamos examinar um exemplo para confirmar a nossa compreensão do problema: suponhamos que a entrada de valores numéricos

Salário bruto| 45.000
---  | ---
prêmio | 10.000
base IAPAS | 30.000
base IR | 50.000
taxa IAPAS | 10%
taxa IR |15%
